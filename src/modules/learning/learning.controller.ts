import {
  Controller,
  Get,
  Post,
  UseGuards,
  UseInterceptors,
  Req,
  UploadedFile,
  Delete,
} from '@nestjs/common';
import { LearningService } from './learning.service';
import { AuthGuard } from 'src/guards/auth/auth.guard';
import { FileInterceptor } from '@nestjs/platform-express';
import { Record, Validation } from 'src/interfaces/file_update';
import { UserAuth } from 'src/interfaces/auth';
import { CreateRecordDto } from '../dto/create-record.dto';
@UseGuards(AuthGuard)
@Controller('learning')
export class LearningController {
  constructor(private readonly learningService: LearningService) {}

  @Post()
  @UseInterceptors(FileInterceptor('file'))
  async created(
    @Req() req: { user: UserAuth; body: CreateRecordDto },
    @UploadedFile() file: Express.Multer.File,
  ) {
    const response: Validation = await this.learningService.validateFile(file);
    if (response.success) {
      await this.learningService.create(response.listObjects);
      const createRecord: Record = {
        ...req.body,
        createdBy: req.user.email,
      };
      await this.learningService.createRecord(createRecord);
    }
    return response;
  }

  @Get()
  findAll() {
    return this.learningService.findAll();
  }

  @Delete()
  delete() {
    return this.learningService.delete();
  }

  @Get('records')
  findAllRecords() {
    return this.learningService.findAllRecords();
  }
}
