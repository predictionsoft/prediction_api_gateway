import { Module } from '@nestjs/common';
import { LearningService } from './learning.service';
import { LearningController } from './learning.controller';
import { Auth } from 'src/providers/auth/auth';
import { HttpModule } from '@nestjs/axios';
import { FileUpdate } from 'src/providers/file_update/file_update';

@Module({
  imports: [HttpModule],
  controllers: [LearningController],
  providers: [Auth, LearningService, FileUpdate],
})
export class LearningModule {}
