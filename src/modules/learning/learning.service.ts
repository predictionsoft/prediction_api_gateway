import { Injectable } from '@nestjs/common';
import { Record, SalesValidator, Validation } from 'src/interfaces/file_update';
import { FileUpdate } from 'src/providers/file_update/file_update';
import * as FormData from 'form-data';
import { Readable } from 'stream';

@Injectable()
export class LearningService {
  constructor(private fileUpdate: FileUpdate) {}

  async validateFile(file: Express.Multer.File): Promise<Validation> {
    const formData = new FormData();
    const readableFile = new Readable();
    readableFile.push(file.buffer);
    readableFile.push(null);
    formData.append('file', readableFile, {
      filename: file.originalname,
      contentType: file.mimetype,
    });
    formData.append('strategy', 'sales');
    return await this.fileUpdate.validLearning(formData);
  }

  async create(sales: Array<SalesValidator>) {
    return await this.fileUpdate.postLearning({ sales });
  }

  async findAll() {
    return await this.fileUpdate.getLearning();
  }

  async createRecord(record: Record) {
    return await this.fileUpdate.postRecord(record);
  }

  async findAllRecords() {
    return await this.fileUpdate.getRecord();
  }

  async delete() {
    await this.fileUpdate.deleteLearning();
    return await this.fileUpdate.deleteRecord();
  }
}
