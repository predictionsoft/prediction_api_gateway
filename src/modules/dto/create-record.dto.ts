import { IsNotEmpty, IsNumber } from 'class-validator';

export class CreateRecordDto {
  @IsNotEmpty()
  @IsNumber()
  month: number;

  @IsNotEmpty()
  @IsNumber()
  year: number;
}
