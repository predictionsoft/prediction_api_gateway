import { Injectable } from '@nestjs/common';
import { MachineLearning } from 'src/providers/machine_learning/machine_learning';

@Injectable()
export class PredictionService {
  constructor(private machineLearning: MachineLearning) {}
  async prediction() {
    await this.machineLearning.prediction();
    return { process: true };
  }
}
