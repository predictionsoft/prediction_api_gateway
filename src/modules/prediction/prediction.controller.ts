import { Controller, Get, UseGuards } from '@nestjs/common';
import { PredictionService } from './prediction.service';
import { AuthGuard } from 'src/guards/auth/auth.guard';

@UseGuards(AuthGuard)
@Controller('prediction')
export class PredictionController {
  constructor(private readonly predictionService: PredictionService) {}

  @Get()
  prediction() {
    return this.predictionService.prediction();
  }
}
