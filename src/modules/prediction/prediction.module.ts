import { Module } from '@nestjs/common';
import { PredictionService } from './prediction.service';
import { PredictionController } from './prediction.controller';
import { Auth } from 'src/providers/auth/auth';
import { HttpModule } from '@nestjs/axios';
import { MachineLearning } from 'src/providers/machine_learning/machine_learning';

@Module({
  imports: [HttpModule],
  controllers: [PredictionController],
  providers: [Auth, PredictionService, MachineLearning],
})
export class PredictionModule {}
