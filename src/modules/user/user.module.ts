import { Module } from '@nestjs/common';
import { UserController } from './user.controller';
import { Auth } from 'src/providers/auth/auth';
import { HttpModule } from '@nestjs/axios';

@Module({
  imports: [HttpModule],
  controllers: [UserController],
  providers: [Auth],
})
export class UserModule {}
