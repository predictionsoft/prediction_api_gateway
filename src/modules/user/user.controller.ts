import { Controller, Get, UseGuards, Req } from '@nestjs/common';
import { AuthGuard } from 'src/guards/auth/auth.guard';
import { UserAuth } from 'src/interfaces/auth';

@UseGuards(AuthGuard)
@Controller('user')
export class UserController {
  @Get()
  getUser(@Req() req: { user: UserAuth }) {
    return req.user;
  }
}
