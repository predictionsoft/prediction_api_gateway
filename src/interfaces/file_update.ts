export interface ResponseLearning {
  data: Array<Learning>;
}

export interface Learning {
  _id: string;
  dateBilling: Date;
  gender: string;
  birthdate: Date;
  age: number;
  amount: number;
  createdAt: Date;
  updatedAt: Date;
}

export interface ResponseValidation {
  data: Validation;
}

export interface Validation {
  listObjects: Array<SalesValidator>;
  errorMapping?: ErrorMapping[];
  errorMessage?: ErrorFiles;
  success: boolean;
}

export interface ErrorMapping {
  reason: string[];
  item: SalesValidator;
}

export interface ErrorFiles {
  title: string;
  message: string;
}

export interface SalesValidator {
  dateBilling: string;
  gender: Gender;
  birthdate: string;
  age?: number;
}

export enum Gender {
  male = 'male',
  female = 'female',
}

export interface Record {
  month: number;
  year: number;
  createdBy: string;
  createdAt?: string;
}

export interface RecordResponse {
  data: Record;
}

export interface RecordsResponse {
  data: Array<Record>;
}

export interface RecordsDeleteResponse {
  data: Delete;
}

export interface Delete {
  acknowledged: boolean;
  deletedCount: number;
}
