export interface UserAuth {
  email: string;
  picture: string;
}
