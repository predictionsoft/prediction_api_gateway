import { Injectable, Logger } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { UserAuth } from 'src/interfaces/auth';

@Injectable()
export class Auth {
  private logger = new Logger(Auth.name);

  private url = process.env.PREDICTION_MC_AUTH;

  constructor(private http: HttpService) {}

  public async validToken(token: string): Promise<UserAuth> {
    try {
      const response: UserAuth = await this.http.axiosRef({
        method: 'GET',
        baseURL: this.url,
        headers: {
          Authorization: `${token}`,
        },
        url: `auth/validate-token`,
      });
      return response;
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }
}
