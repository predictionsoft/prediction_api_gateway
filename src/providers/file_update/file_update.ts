import { Injectable, Logger } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import {
  Delete,
  Learning,
  Record,
  RecordResponse,
  RecordsDeleteResponse,
  RecordsResponse,
  ResponseLearning,
  ResponseValidation,
  SalesValidator,
  Validation,
} from 'src/interfaces/file_update';

@Injectable()
export class FileUpdate {
  private logger = new Logger(FileUpdate.name);

  private url = process.env.PREDICTION_MC_FILE_UPDATE;

  constructor(private http: HttpService) {}

  public async getLearning(): Promise<Array<Learning>> {
    try {
      const { data: response }: ResponseLearning = await this.http.axiosRef({
        method: 'GET',
        baseURL: this.url,
        url: `learning-save`,
      });
      return response;
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  public async validLearning(data: unknown): Promise<Validation> {
    try {
      const { data: response }: ResponseValidation = await this.http.axiosRef({
        method: 'POST',
        baseURL: this.url,
        url: `validator`,
        data,
      });
      return response;
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  public async postLearning(data: {
    sales: Array<SalesValidator>;
  }): Promise<Array<Learning>> {
    try {
      const { data: response }: ResponseLearning = await this.http.axiosRef({
        method: 'POST',
        baseURL: this.url,
        url: `learning-save`,
        data,
      });
      return response;
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  public async deleteLearning(): Promise<Delete> {
    try {
      const { data: response }: RecordsDeleteResponse =
        await this.http.axiosRef({
          method: 'Delete',
          baseURL: this.url,
          url: `learning-save`,
        });
      return response;
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  public async postRecord(data: Record): Promise<Record> {
    try {
      const { data: response }: RecordResponse = await this.http.axiosRef({
        method: 'POST',
        baseURL: this.url,
        url: `record`,
        data,
      });
      return response;
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  public async getRecord(): Promise<Array<Record>> {
    try {
      const { data: response }: RecordsResponse = await this.http.axiosRef({
        method: 'Get',
        baseURL: this.url,
        url: `record`,
      });
      return response;
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  public async deleteRecord(): Promise<Delete> {
    try {
      const { data: response }: RecordsDeleteResponse =
        await this.http.axiosRef({
          method: 'Delete',
          baseURL: this.url,
          url: `record`,
        });
      return response;
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }
}
