import { Injectable, Logger } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';

@Injectable()
export class MachineLearning {
  private logger = new Logger(MachineLearning.name);

  private url = process.env.PREDICTION_MC_MACHINE_LEARNING;

  constructor(private http: HttpService) {}

  public async prediction(): Promise<void> {
    try {
      await this.http.axiosRef({
        method: 'GET',
        baseURL: this.url,
        url: `api/v1/sales`,
      });
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }
}
