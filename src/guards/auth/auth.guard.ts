import {
  Injectable,
  ExecutionContext,
  UnauthorizedException,
} from '@nestjs/common';
import { CanActivate } from '@nestjs/common';
import { Observable, from } from 'rxjs';
import { map } from 'rxjs/operators';
import { UserAuth } from 'src/interfaces/auth';
import { Auth } from 'src/providers/auth/auth';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private readonly auth: Auth) {}

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const request = context.switchToHttp().getRequest();
    const authorizationHeader = request.headers['authorization'];
    if (!authorizationHeader) {
      throw new UnauthorizedException();
    }
    return from(this.auth.validToken(authorizationHeader)).pipe(
      map((user: UserAuth) => {
        if (!user) {
          throw new UnauthorizedException();
        }
        const userWithoutCircularReferences = {
          email: user['data'].email,
          picture: user['data'].picture,
        };
        request.user = userWithoutCircularReferences;
        return true;
      }),
    );
  }
}
