import { Module } from '@nestjs/common';
import { Auth } from './providers/auth/auth';
import { ConfigModule } from '@nestjs/config';
import { LearningModule } from './modules/learning/learning.module';
import { PredictionModule } from './modules/prediction/prediction.module';
import { HttpModule } from '@nestjs/axios';
import { UserModule } from './modules/user/user.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    HttpModule,
    LearningModule,
    PredictionModule,
    UserModule,
  ],
  controllers: [],
  providers: [Auth],
})
export class AppModule {}
